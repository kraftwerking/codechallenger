package com.kraftwerking.codechallenger.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class Person {
	
	private String lastName;
	private String firstName;
	private String sex;
	private String favoriteColor;
	private String birthDate;
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getFavoriteColor() {
		return favoriteColor;
	}
	public void setFavoriteColor(String favoriteColor) {
		this.favoriteColor = favoriteColor;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	@Override
	public String toString() {
		return "Person [lastName=" + lastName + ", firstName=" + firstName + ", sex=" + sex + ", favoriteColor="
				+ favoriteColor + ", birthDate=" + birthDate + "]";
	}
	
	public String toJsonString() throws JsonProcessingException{
		ObjectMapper mapperObj = new ObjectMapper();

        // get Person object as a json string
        String jsonStr = mapperObj.writeValueAsString(this);
		return jsonStr;
	}
	
}
