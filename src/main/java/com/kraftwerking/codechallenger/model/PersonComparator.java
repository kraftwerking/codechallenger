package com.kraftwerking.codechallenger.model;

import java.util.Comparator;

import org.springframework.stereotype.Component;

@Component
public class PersonComparator implements Comparator<Person> {

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	private String sortBy;

	@Override
	public int compare(Person person1, Person person2) {
		switch (sortBy) {
		case "SexAndLastName":
			String x1 = ((Person) person1).getSex();
			String x2 = ((Person) person2).getSex();
			int sComp = x1.compareTo(x2);
			if (sComp != 0) {
				return sComp;
			} else {
				return person1.getLastName().compareTo(person2.getLastName());
			}
		case "LastName":
			return person1.getLastName().compareTo(person2.getLastName());
		case "BirthDate":
			return person1.getBirthDate().compareTo(person2.getBirthDate());
		case "Sex":
			return person1.getSex().compareTo(person2.getSex());
		}
		throw new RuntimeException("Practically unreachable code, can't be thrown");
	}

	public PersonComparator(String sortBy) {
		super();
		this.sortBy = sortBy;
	}

	public PersonComparator() {
		super();
		this.sortBy = "LastName";
	}

}