package com.kraftwerking.codechallenger.filereader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import javax.annotation.Resource;

import org.json.JSONArray;
import org.springframework.stereotype.Component;

import com.kraftwerking.codechallenger.model.Person;
import com.kraftwerking.codechallenger.model.PersonComparator;

@Component
public class FileReaderUtils {

	private static final String COMMA_DELIMITER = ", ";
	private static final String PIPE_DELIMITER = "\\| ";
	private static final String SPACE_DELIMITER = " ";

	@Resource
	Person tmpPerson;

	@Resource
	PersonComparator comparator = new PersonComparator();

	public String sortFile(String sortBy, String delim) throws FileNotFoundException {
		Scanner scanner = null;
		comparator.setSortBy(sortBy);
		if (delim.equals("comma")) {
			scanner = new Scanner(new File("src/main/resources/data/input_csv"));
			scanner.useDelimiter("[\\r\\n;]|" + COMMA_DELIMITER);
		} else if (delim.equals("pipe")) {
			scanner = new Scanner(new File("src/main/resources/data/input_pipe"));
			scanner.useDelimiter("[\\r\\n;]|" + PIPE_DELIMITER);
		} else {
			scanner = new Scanner(new File("src/main/resources/data/input_space"));
			scanner.useDelimiter("[\\r\\n;]|" + SPACE_DELIMITER);
		}

		List<Person> people = new ArrayList<Person>();
		int count = 0;

		while (scanner.hasNext()) {
			count++;
			if (count == 1) {
				tmpPerson.setLastName(scanner.next());
			} else if (count == 2) {
				tmpPerson.setFirstName(scanner.next());
			} else if (count == 3) {
				tmpPerson.setSex(scanner.next());
			} else if (count == 4) {
				tmpPerson.setFavoriteColor(scanner.next());
			} else if (count == 5) {
				tmpPerson.setBirthDate(scanner.next());
				// System.out.println(tmpPerson.toJsonString());
				people.add(tmpPerson);
				tmpPerson = new Person();
				count = 0;
			}
		}
		Collections.sort(people, comparator); 

		JSONArray jsonArray = new JSONArray(people);
		return jsonArray.toString();
	}

	public String appendToFile(String fileName, String decodedValue) throws IOException {
		// TODO Auto-generated method stub
		Writer output;
		output = new BufferedWriter(new FileWriter(fileName, true));
		output.append(decodedValue);
		output.close();
		return output.toString();
		
	}

	public String readFile(String string) throws FileNotFoundException {
		// TODO Auto-generated method stub
		File file = new File("src/main/resources/data/input_web");
		String contents = "Contents of file: \n" + new Scanner(file).useDelimiter("\\Z").next();
		return contents;
	}

}