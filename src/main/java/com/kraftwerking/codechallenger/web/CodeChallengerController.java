/**
 * 
 */
package com.kraftwerking.codechallenger.web;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kraftwerking.codechallenger.filereader.FileReaderUtils;

/**
 * @author rj
 *
 */

@RestController
public class CodeChallengerController {

	// TODO: not all file formats are covered in REST api

	@Resource
	FileReaderUtils fileReaderUtils;

	@RequestMapping(value = "/records/gender", method = RequestMethod.GET)
	@ResponseBody
	public String getBySexAndLastName() throws FileNotFoundException {
		return fileReaderUtils.sortFile("SexAndLastName", "comma");
	}

	@RequestMapping(value = "/records/birthdate", method = RequestMethod.GET)
	@ResponseBody
	public String getByBirthDate() throws FileNotFoundException {
		return fileReaderUtils.sortFile("BirthDate", "comma");
	}

	@RequestMapping(value = "/records/name", method = RequestMethod.GET)
	@ResponseBody
	public String getByLastName() throws FileNotFoundException {
		return fileReaderUtils.sortFile("LastName", "comma");
	}

	@RequestMapping(value = "/records", method = RequestMethod.POST)
	@ResponseBody
	public String postRecord(@RequestBody String record) throws UnsupportedEncodingException, IOException {
		String decodedValue = URLDecoder.decode(record, "UTF-8");
		decodedValue = decodedValue.substring(0, decodedValue.length() - 1);
		System.out.println(decodedValue);

		fileReaderUtils.appendToFile("src/main/resources/data/input_web", decodedValue);
		String contents = fileReaderUtils.readFile("src/main/resources/data/input_web");
		return contents;
	}

}
