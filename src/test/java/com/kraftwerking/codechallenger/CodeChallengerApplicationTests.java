package com.kraftwerking.codechallenger;

import static org.junit.Assert.assertTrue;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.kraftwerking.codechallenger.filereader.FileReaderUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CodeChallengerApplicationTests {

	@Resource
	FileReaderUtils fileReaderUtils;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testSortBySex() throws FileNotFoundException {
		String res = fileReaderUtils.sortFile("SexAndLastName", "comma");
		String expected = "[{\"lastName\":\"Barton\",\"firstName\":\"Paula\",\"sex\":\"F\",\"favoriteColor\":\"Pink\",\"birthDate\":\"08/22/02\"},{\"lastName\":\"Smith\",\"firstName\":\"Maria\",\"sex\":\"F\",\"favoriteColor\":\"Green\",\"birthDate\":\"09/07/07\"},{\"lastName\":\"Adams\",\"firstName\":\"Ted\",\"sex\":\"M\",\"favoriteColor\":\"Blue\",\"birthDate\":\"08/08/08\"},{\"lastName\":\"Jones\",\"firstName\":\"Jim\",\"sex\":\"M\",\"favoriteColor\":\"Red\",\"birthDate\":\"11/10/10\"}]";
		assertTrue(res.equals(expected));
	}

	@Test
	public void testSortByLastName() throws FileNotFoundException {
		String expected = "[{\"lastName\":\"Adams\",\"firstName\":\"Ted\",\"sex\":\"M\",\"favoriteColor\":\"Blue\",\"birthDate\":\"08/08/08\"},{\"lastName\":\"Barton\",\"firstName\":\"Paula\",\"sex\":\"F\",\"favoriteColor\":\"Pink\",\"birthDate\":\"08/22/02\"},{\"lastName\":\"Jones\",\"firstName\":\"Jim\",\"sex\":\"M\",\"favoriteColor\":\"Red\",\"birthDate\":\"11/10/10\"},{\"lastName\":\"Smith\",\"firstName\":\"Maria\",\"sex\":\"F\",\"favoriteColor\":\"Green\",\"birthDate\":\"09/07/07\"}]";
		String res = fileReaderUtils.sortFile("LastName", "comma");
		assertTrue(res.equals(expected));

	}

	@Test
	public void testSortByBirthDate() throws FileNotFoundException {
		String expected = "[{\"lastName\":\"Adams\",\"firstName\":\"Ted\",\"sex\":\"M\",\"favoriteColor\":\"Blue\",\"birthDate\":\"08/08/08\"},{\"lastName\":\"Barton\",\"firstName\":\"Paula\",\"sex\":\"F\",\"favoriteColor\":\"Pink\",\"birthDate\":\"08/22/02\"},{\"lastName\":\"Smith\",\"firstName\":\"Maria\",\"sex\":\"F\",\"favoriteColor\":\"Green\",\"birthDate\":\"09/07/07\"},{\"lastName\":\"Jones\",\"firstName\":\"Jim\",\"sex\":\"M\",\"favoriteColor\":\"Red\",\"birthDate\":\"11/10/10\"}]";
		String res = fileReaderUtils.sortFile("BirthDate", "comma");
		assertTrue(res.equals(expected));

	}

	@Test
	public void testAppendToFile() throws IOException {
		int i = countLines("src/main/resources/data/input_web");
		System.out.println(i);

		String record = "Jones, Jim, M, Red, 11/10/10\n";

		fileReaderUtils.appendToFile("src/main/resources/data/input_web", record);

		int j = countLines("src/main/resources/data/input_web");
		System.out.println(j);
		assertTrue(i < j);

	}

	public static int countLines(String filename) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(filename));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		} finally {
			is.close();
		}
	}

}
